// OUR SERVICES

const servicesHeader = document.getElementsByClassName('main-services-block-header-item');
const servicesItem = document.getElementsByClassName('main-services-block-content');

for(let i = 0; i < servicesHeader.length; i++){
    servicesHeader[i].addEventListener('click', function (){
        const remuveClassesHeader = document.getElementsByClassName('main-services-block-header-item-chosen');
        const remuveClassesItem = document.getElementsByClassName('main-services-block-content-chosen');
        for(let x = 0; x < remuveClassesHeader.length; x++){
            remuveClassesHeader[x].classList.remove('main-services-block-header-item-chosen');
        }
        for(let y = 0; y < remuveClassesItem.length; y++){
            remuveClassesItem[y].classList.remove('main-services-block-content-chosen');
        }
        servicesHeader[i].classList.add('main-services-block-header-item-chosen');
        servicesItem[i].classList.add('main-services-block-content-chosen');

    })
}

// OUR SERVICES

// OUR AMAZING WORK

const imgMass = [
    { tab: 'Graphic Design', img: 'img/ourWork/graphic%20design/graphic-design1.jpg', name: 'Ambrella' },
    { tab: 'Graphic Design', img: 'img/ourWork/graphic%20design/graphic-design2.jpg', name: 'Radyga' },
    { tab: 'Graphic Design', img: 'img/ourWork/graphic%20design/graphic-design3.jpg', name: 'Cup' },
    { tab: 'Graphic Design', img: 'img/ourWork/graphic%20design/graphic-design4.jpg', name: 'Shues' },
    { tab: 'Graphic Design', img: 'img/ourWork/graphic%20design/graphic-design5.jpg', name: 'light' },
    { tab: 'Graphic Design', img: 'img/ourWork/graphic%20design/graphic-design6.jpg', name: 'we thrust' },
    { tab: 'Graphic Design', img: 'img/ourWork/graphic%20design/graphic-design7.jpg', name: 'Design Here' },
    { tab: 'Graphic Design', img: 'img/ourWork/graphic%20design/graphic-design8.jpg', name: 'yeah' },
    { tab: 'Graphic Design', img: 'img/ourWork/graphic%20design/graphic-design9.jpg', name: 'lists' },
    { tab: 'Graphic Design', img: 'img/ourWork/graphic%20design/graphic-design10.jpg', name: 'study' },
    { tab: 'Graphic Design', img: 'img/ourWork/graphic%20design/graphic-design11.jpg', name: 'desck' },
    { tab: 'Graphic Design', img: 'img/ourWork/graphic%20design/graphic-design12.jpg', name: 'pc' },
    { tab: 'Web Design', img: 'img/ourWork/web%20design/web-design1.jpg', name: 'phone' },
    { tab: 'Web Design', img: 'img/ourWork/web%20design/web-design2.jpg', name: 'tab' },
    { tab: 'Web Design', img: 'img/ourWork/web%20design/web-design3.jpg', name: 'notebook' },
    { tab: 'Web Design', img: 'img/ourWork/web%20design/web-design4.jpg', name: 'notebook 2' },
    { tab: 'Web Design', img: 'img/ourWork/web%20design/web-design5.jpg', name: 'notebook 3' },
    { tab: 'Web Design', img: 'img/ourWork/web%20design/web-design6.jpg', name: 'notebook 4' },
    { tab: 'Web Design', img: 'img/ourWork/web%20design/web-design7.jpg', name: 'link' },
    { tab: 'Landing Pages', img: 'img/ourWork/landing%20page/landing-page1.jpg', name: 'page' },
    { tab: 'Landing Pages', img: 'img/ourWork/landing%20page/landing-page2.jpg', name: 'page 2' },
    { tab: 'Landing Pages', img: 'img/ourWork/landing%20page/landing-page3.jpg', name: 'page 3' },
    { tab: 'Landing Pages', img: 'img/ourWork/landing%20page/landing-page4.jpg', name: 'page 4' },
    { tab: 'Landing Pages', img: 'img/ourWork/landing%20page/landing-page5.jpg', name: 'page 5' },
    { tab: 'Landing Pages', img: 'img/ourWork/landing%20page/landing-page6.jpg', name: 'page 6' },
    { tab: 'Landing Pages', img: 'img/ourWork/landing%20page/landing-page7.jpg', name: 'page 7' },
    { tab: 'Wordpress', img: 'img/ourWork/wordpress/wordpress1.jpg', name: 'press 1' },
    { tab: 'Wordpress', img: 'img/ourWork/wordpress/wordpress2.jpg', name: 'press 2' },
    { tab: 'Wordpress', img: 'img/ourWork/wordpress/wordpress3.jpg', name: 'press 3' },
    { tab: 'Wordpress', img: 'img/ourWork/wordpress/wordpress4.jpg', name: 'press 4' },
    { tab: 'Wordpress', img: 'img/ourWork/wordpress/wordpress5.jpg', name: 'press 5' },
    { tab: 'Wordpress', img: 'img/ourWork/wordpress/wordpress6.jpg', name: 'press 6' },
    { tab: 'Wordpress', img: 'img/ourWork/wordpress/wordpress7.jpg', name: 'press 7' },
    { tab: 'Wordpress', img: 'img/ourWork/wordpress/wordpress8.jpg', name: 'press 8' },
    { tab: 'Wordpress', img: 'img/ourWork/wordpress/wordpress9.jpg', name: 'press 9' },
    { tab: 'Wordpress', img: 'img/ourWork/wordpress/wordpress10.jpg', name: 'press 10' },
];

const getMoreData = (tab) => {
    if (tab) {
        return imgMass.filter(item => item.tab.toLowerCase() === tab).slice(12, 24);
    }
    return imgMass.slice(12, 24);
}

const getFilterSlice = (tab)=>{
    if (tab === 'all'){
        return imgMass;
    } else {
        return imgMass.filter(item => item.tab.toLowerCase() === tab);
    }
}

const loadData = () => {
    document.getElementsByClassName('main-work-block')[0].innerHTML += getMoreData().reduce((moreImg, value) => {
        moreImg += `<div data-tab=${value.tab} class="main-work-item">
                    <img class="main-work-img" src=${value.img}>
                    <div class="main-work-content">
                        <div class="main-work-links">
                            <a class="main-work-svg-first"><svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M13.9131 2.72817L12.0948 0.891285C11.2902 0.0808612 9.98305 0.0759142 9.17681 0.882615L7.15921 2.89256C6.35161 3.69885 6.34818 5.01032 7.15051 5.82074L8.30352 4.68897C8.18678 4.32836 8.33041 3.9153 8.61593 3.62946L9.89949 2.35187C10.3061 1.94624 10.9584 1.94913 11.3595 2.35434L12.4513 3.45805C12.8528 3.86283 12.8511 4.51713 12.447 4.92318L11.1634 6.20241C10.8918 6.47296 10.4461 6.62168 10.1002 6.52626L8.97094 7.65887C9.77453 8.47177 11.0803 8.47466 11.8889 7.66837L13.9039 5.65924C14.7141 4.85254 14.7167 3.53983 13.9131 2.72817ZM6.52613 10.0918C6.62191 10.4441 6.46857 10.8997 6.19093 11.1777L4.99227 12.3752C4.58074 12.7845 3.91595 12.7833 3.50671 12.369L2.39297 11.2475C1.98465 10.8349 1.98729 10.1633 2.39824 9.75473L3.59804 8.55769C3.89032 8.26607 4.31044 8.12025 4.67711 8.24375L5.83354 7.0715C5.01493 6.2462 3.68249 6.24207 2.86059 7.06324L0.915197 9.0042C0.0922615 9.8266 0.0883685 11.1629 0.90651 11.9886L2.75817 13.8618C3.57595 14.6846 4.90724 14.6912 5.73111 13.8701L7.67649 11.9287C8.49852 11.1054 8.5024 9.77166 7.68553 8.9443L6.52613 10.0918ZM6.25787 9.56307C5.98013 9.84189 5.53427 9.84105 5.26179 9.55812C4.98792 9.27434 4.9901 8.82039 5.26613 8.53993L8.59075 5.16109C8.86679 4.88227 9.31174 4.88311 9.58513 5.16398C9.86048 5.44569 9.85876 5.90088 9.5817 6.18299L6.25787 9.56307Z" fill="#1FDAB5"/>
                            </svg></a>
                            <a class="main-work-svg-second"><div class="main-work-svg-sec-qudr"> </div> </a>
                        </div>
                        <h5 class='main-work-h-name'>${value.name}</h5>
                        <h6 class="main-work-h-tab">${value.tab}</h6>
                    </div>
                </div>`
        document.getElementById('btn-load-more').classList.add('load-more-div-btn-none')
        return moreImg;
    }, '');
}

document.addEventListener('DOMContentLoaded', ()=>{createNewImg('all')});


let createNewImg = function (tab){

    document.getElementsByClassName('main-work-block')[0].innerHTML = getFilterSlice(tab).slice(0, 12).reduce((moreImg, value) => {
        moreImg += `<div data-tab=${value.tab} class="main-work-item">
                    <img class="main-work-img" src=${value.img}>
                    <div class="main-work-content">
                        <div class="main-work-links">
                            <a class="main-work-svg-first"><svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M13.9131 2.72817L12.0948 0.891285C11.2902 0.0808612 9.98305 0.0759142 9.17681 0.882615L7.15921 2.89256C6.35161 3.69885 6.34818 5.01032 7.15051 5.82074L8.30352 4.68897C8.18678 4.32836 8.33041 3.9153 8.61593 3.62946L9.89949 2.35187C10.3061 1.94624 10.9584 1.94913 11.3595 2.35434L12.4513 3.45805C12.8528 3.86283 12.8511 4.51713 12.447 4.92318L11.1634 6.20241C10.8918 6.47296 10.4461 6.62168 10.1002 6.52626L8.97094 7.65887C9.77453 8.47177 11.0803 8.47466 11.8889 7.66837L13.9039 5.65924C14.7141 4.85254 14.7167 3.53983 13.9131 2.72817ZM6.52613 10.0918C6.62191 10.4441 6.46857 10.8997 6.19093 11.1777L4.99227 12.3752C4.58074 12.7845 3.91595 12.7833 3.50671 12.369L2.39297 11.2475C1.98465 10.8349 1.98729 10.1633 2.39824 9.75473L3.59804 8.55769C3.89032 8.26607 4.31044 8.12025 4.67711 8.24375L5.83354 7.0715C5.01493 6.2462 3.68249 6.24207 2.86059 7.06324L0.915197 9.0042C0.0922615 9.8266 0.0883685 11.1629 0.90651 11.9886L2.75817 13.8618C3.57595 14.6846 4.90724 14.6912 5.73111 13.8701L7.67649 11.9287C8.49852 11.1054 8.5024 9.77166 7.68553 8.9443L6.52613 10.0918ZM6.25787 9.56307C5.98013 9.84189 5.53427 9.84105 5.26179 9.55812C4.98792 9.27434 4.9901 8.82039 5.26613 8.53993L8.59075 5.16109C8.86679 4.88227 9.31174 4.88311 9.58513 5.16398C9.86048 5.44569 9.85876 5.90088 9.5817 6.18299L6.25787 9.56307Z" fill="#1FDAB5"/>
                            </svg></a>
                            <a class="main-work-svg-second"><div class="main-work-svg-sec-qudr"> </div> </a>
                        </div>
                        <h5 class='main-work-h-name'>${value.name}</h5>
                        <h6 class="main-work-h-tab">${value.tab}</h6>
                    </div>
                </div>`

        if (getFilterSlice(tab).length <= 12){
            document.getElementById('btn-load-more').classList.add('load-more-div-btn-none')
        } else document.getElementById('btn-load-more').classList.remove('load-more-div-btn-none')
        return moreImg;
    }, '');

};


const workHeader = document.getElementsByClassName('main-our-work-items');
for(let j = 0; j < workHeader.length; j++){
    workHeader[j].addEventListener('click', function (){
        const remuveWorkClassesHeader = document.getElementsByClassName('main-our-work-items-chosen');
        for(let x = 0; x < remuveWorkClassesHeader.length; x++){
            remuveWorkClassesHeader[x].classList.remove('main-our-work-items-chosen');
        }
        workHeader[j].classList.add('main-our-work-items-chosen');

        createNewImg(workHeader[j].textContent.toLowerCase())
    })
}

document.getElementsByClassName('load-more-btn')[0].addEventListener('click', loadData);

// OUR AMAZING WORK

// BREACKING NEWS

const breakingNews = document.getElementById('breacking-conteiner')

breakingNews.addEventListener('mouseover', function(event) {breackingHover(event)});
breakingNews.addEventListener('mouseout', function(event) {breackingHover(event)});

let breackingHover = function (event){
    let eventA = event.target.closest('a');
    if (eventA == null) return
    let eventDate = eventA.getElementsByClassName('main-breacking-content-date')[0]
    eventDate.classList.toggle('main-breacking-content-date-hover')
    let eventSpan = eventA.getElementsByClassName('main-breacking-content-text-first-str')[0]
    eventSpan.classList.toggle('main-breacking-content-text-first-str-hover')
}

// BREACKING NEWS

$(document).ready(function (){
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        dots: false,
        centerMode: false,
        focusOnSelect: true,
        // appendArrows:$('.slider-btn')
        prevArrow: '.slider-btn-prev',
        nextArrow: '.slider-btn-next'
    });
})